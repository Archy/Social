#!/usr/bin/python
import sys
import Image
import ImageDraw
import ImageFont
import sqlite3
import os

#Script is used to convert text from database in sqlite into a picture jpeg format
# if (len(sys.argv) != 2):
#	print "Invalid number of arguments. Needed only one"
#	print "Write text as a parameter."
#	sys.exit()

#settings
font_size = 40
border_size = 3
font_path = "./coda/Coda-Heavy.ttf"
db_path = sys.argv[1]
images_path = './'

conn = sqlite3.connect(db_path)
c = conn.cursor()
#which tables
table_names = ['expectations', 'workgroups', 'expertises']

os.mkdir("./textImages", 0755)
for table_name in table_names:
	os.mkdir("./textImages/"+table_name, 0755)

	list_of_words = c.execute('select name from '+table_name).fetchall()
	list_of_words = [i[0] for i in list_of_words]
	list_of_words = [i.encode('ascii','ignore') for i in list_of_words]

	for word in list_of_words:

		#code
		fnt = ImageFont.truetype(font_path, font_size)
		x_sum=0;

		for letter in word:
			(x,y) = fnt.getsize(letter)
			x_sum+=x
		img = Image.new('RGBA', (x_sum,int(font_size*2)+border_size), (255,255,255,0))
		basic_img = Image.new('RGBA', (x_sum,int(font_size*1.25)+border_size), (255,255,255,0))
		d = ImageDraw.Draw(img)
		d_basic = ImageDraw.Draw(basic_img)

		for i in range(0,border_size):
			for j in range (0,border_size):
				d.text((i,j), word, font=fnt, fill=(255,0,0,255))

		d.text((border_size//2,border_size//2), word, font=fnt, fill=(43,10,85,255))
		d_basic.text((border_size//2,border_size//2), word, font=fnt, fill=(43,10,85,255))

		fp = "./textImages/"+table_name+"/highlighted."+word+".png"
		img.save(open(fp, 'w'))
	
		fp = "./textImages/"+table_name+"/"+word+".png"
		basic_img.save(open(fp, 'w'))


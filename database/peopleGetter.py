#!/usr/bin/python
import sqlite3
import codecs, json

#open database
conn = sqlite3.connect('keystone.db')
print "Opened database successfully"

people_cursor = conn.execute("SELECT id, name, surname, picture  from people")
peopleTable = []
#iterate over people
for person in people_cursor:
	personData = {'ID':person[0], 'name': person[1] + " " + person[2], 'picture': person[3]}
	print personData
	peopleTable.append(personData)
	#print "ID = ", person[0]
	#print "NAME = ", person[1]
	#print "surname = ", person[2]
	#print "picture = ", person[3], "\n"

json.dump(peopleTable, codecs.open('people.json', 'w', encoding='utf-8'), separators=(',', ':'), sort_keys=False, indent=3)
#close database
print "Operation done successfully"
conn.close()
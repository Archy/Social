#!/usr/bin/python
import sqlite3
import codecs, json
import numpy as np

#open database
conn = sqlite3.connect('keystone.db')
print "Opened database successfully"

table = []
#iterate over expectations
types = []
cursor = conn.execute("SELECT id, name  from expectations")
for row in cursor:
	pass
	types.append({'ID': row[0],'name': row[1]})

table.append({'name':'expectations', 'types':types})

#iterate over expertises
types = []
cursor = conn.execute("SELECT id, name  from expertises")
for row in cursor:
	pass
	types.append({'ID': row[0],'name': row[1]})

table.append({'name':'expertises', 'types':types})


#iterate over workgroups
types = []
cursor = conn.execute("SELECT id, name  from workgroups")
for row in cursor:
	pass
	types.append({'ID': row[0],'name': row[1]})

table.append({'name':'workgroups', 'types':types})



json.dump(table, codecs.open('interaction.json', 'w', encoding='utf-8'), separators=(',', ':'), sort_keys=False, indent=3)

#jsonMatrix = workgroupsAdMatrix.tolist()
#json.dump(jsonMatrix, codecs.open('graph.json', 'w', encoding='utf-8'), separators=(',', ':'), sort_keys=False, indent=3)

#close database
print "Operation done successfully"
conn.close()

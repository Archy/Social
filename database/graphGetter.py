#!/usr/bin/python
import sqlite3
import codecs, json
import numpy as np

#open database
conn = sqlite3.connect('keystone.db')
print "Opened database successfully"

#create 2 dimensional array for adjacency matrix
peopleCount = (conn.execute("SELECT COUNT(*) from people").fetchone())[0]
expectationsCount = (conn.execute("SELECT COUNT(*) from expectations").fetchone())[0]
expertisesCount = (conn.execute("SELECT COUNT(*) from expertises").fetchone())[0]
workgroupsCount = (conn.execute("SELECT COUNT(*) from workgroups").fetchone())[0]

expectationsAdMatrix = np.zeros((peopleCount,expectationsCount), dtype=bool)
expertisesAdMatrix = np.zeros((peopleCount,expertisesCount), dtype=bool)
workgroupsAdMatrix = np.zeros((peopleCount,workgroupsCount), dtype=bool)


#iterate over people
people_cursor = conn.execute("SELECT id, name, surname, picture  from people")
for person in people_cursor:
	personID = person[0]
	#print "ID = ", person[0]
	#print "NAME = ", person[1]
	#print "surname = ", person[2]
	#print "picture = ", person[3], "\n"

	#iterate over expectations
	cursor = conn.execute("SELECT id, name  from expectations")
	for row in cursor:
		ID = row[0]
		edge = (conn.execute("SELECT COUNT(*) from peop_expec WHERE peop_id=? AND expec_id=?", (personID, ID)).fetchone())[0]
		if edge == 1:
			expectationsAdMatrix[personID-1][ID-1] = True

   #iterate over expertises
	cursor = conn.execute("SELECT id, name  from expertises")
	for row in cursor:
		ID = row[0]
		edge = (conn.execute("SELECT COUNT(*) from peop_exper WHERE peop_id=? AND exper_id=?", (personID, ID)).fetchone())[0]
		if edge == 1:
			expertisesAdMatrix[personID-1][ID-1] = True

   #iterate over workgroups
 	cursor = conn.execute("SELECT id, name  from workgroups")
	for row in cursor:
		ID = row[0]
		edge = (conn.execute("SELECT COUNT(*) from peop_wg WHERE peop_id=? AND wg_id=?", (personID, ID)).fetchone())[0]
		if edge == 1:
			workgroupsAdMatrix[personID-1][ID-1] = True 


#save adjacency matrix
hashTable = {}
hashTable['expectations'] = expectationsAdMatrix.tolist()
hashTable['expertises'] = expertisesAdMatrix.tolist()
hashTable['workgroups'] = workgroupsAdMatrix.tolist()
json.dump(hashTable, codecs.open('graph.json', 'w', encoding='utf-8'), separators=(',', ':'), sort_keys=False, indent=3)

#jsonMatrix = workgroupsAdMatrix.tolist()
#json.dump(jsonMatrix, codecs.open('graph.json', 'w', encoding='utf-8'), separators=(',', ':'), sort_keys=False, indent=3)

#close database
print "Operation done successfully"
conn.close()

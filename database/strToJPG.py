import sys
import Image
import ImageDraw
import ImageFont

#Script is used to convert text into a picture jpeg format
if (len(sys.argv) != 2):
	print "Script is used to convert text into a picture jpeg format."
	print "Invalid number of arguments. Needed only one"
	print "Write text as a parameter."
	sys.exit()

#settings
font_size = 40
border_size = 5
font_path = "./coda/Coda-Heavy.ttf"
word = sys.argv[1]

#code
fnt = ImageFont.truetype(font_path, font_size)
x_sum=0;

for letter in word:
	(x,y) = fnt.getsize(letter)
	x_sum+=x
img = Image.new('RGBA', (x_sum,int(font_size*2)+border_size), (255,255,255,0))
d = ImageDraw.Draw(img)

for i in range(0,border_size):
	for j in range (0,border_size):
		d.text((i,j), word, font=fnt, fill=(255,0,0,255))
d.text((border_size//2,border_size//2), word, font=fnt, fill=(0,0,0,255))

fp = "./"+word+".png"
img.save(open(fp, 'w'))

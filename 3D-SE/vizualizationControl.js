﻿angular.module('socialViz')
    .directive('ngVizualization', function ($q, syncSRV) {
        return {
            restrict: 'EA',
            link: function postLink(scope, element, attrs) {

                var scene, camera, renderer, canvas, modelCreator;
                var font = null;
                var contW = element.parent()[0].clientWidth, contH = element.parent()[0].clientHeight,
                            windowHalfX = contW / 2, windowHalfY = contH / 2;

                var radA = 7, radB = 12;

                var mouseX = 0, mouseY = 0;
                var mouseDown = false, mauseMoved = false;

                var rotationDest, rotationInterval;
                var angleV = 0.06;

                var changeInterval;
                var radV = 0.10;

                //TODO remove
                //https://github.com/mrdoob/stats.js/
                var stats = {};

                movingCamera = new movingCameraClass();
                modelCreator = new ModelsCreator(radA, radB, 2);

                // -----------------------------------
                // Init
                // -----------------------------------
                scope.init = function () {

                    //canvas
                    canvas = document.createElement('canvas');
                    canvas.width = contW;
                    canvas.height = contH;
                    canvas.position = 'relative';

                    //scene
                    scene = new THREE.Scene();

                    //camera
                    camera = new THREE.PerspectiveCamera(20, contW / contH, 0.1, 10000);
                    camera.position.copy(movingCamera.position);
                    camera.up.copy(movingCamera.up);

                    camera.lookAt(new THREE.Vector3(0, 0, 0));
                    scene.add(camera);

                    //renderer
                    renderer = new THREE.WebGLRenderer({ antialias: true });
                    renderer.setSize(contW, contH);
                    renderer.setClearColor(0xf5f5f0);
                    // element is provided by the angular directive
                    element[0].appendChild(renderer.domElement);

                    //events listeners
                    document.addEventListener('mousemove', scope.onDocumentMouseMove, false);
                    document.addEventListener('mouseup', scope.onMouseUnclick, false);
                    element.bind('mousedown', scope.onMouseClick);
                    element.bind('dblclick', scope.onMouseDoubleClick);

                    element[0].addEventListener('DOMMouseScroll', scope.mouseScroll, false); // For FF and Opera
                    element[0].addEventListener('mousewheel', scope.mouseScroll, false); // For others

                    window.addEventListener('resize', scope.onWindowResize, false);

                    //create spheres
                    modelCreator.createSpheres(scene);

                    //TODO delete
                    stats = new Stats();
                    stats.showPanel(0); // 0: fps, 1: ms, 2: mb, 3+: custom
                    document.body.appendChild(stats.dom);


                    renderer.render(scene, camera);
                    console.log('three.js initialized');
                };

                // -----------------------------------
                // Promises
                // -----------------------------------
                scope.$watch('$viewContentLoaded', function () {    //prevents $q.all being fired on empty promises array
                    //load font
                    scope.allPromises.push(new Promise(function (fulfill, reject){
                        var loader = new THREE.FontLoader();
                        loader.load(
                            // resource URL
                            'helvetiker_regular.typeface.json',
                            // Function when resource is loaded
                            function (loadedFont) {
                                font = loadedFont;
                                console.log('font loaded');
                                fulfill(loadedFont);
                            },
                            // Function called when download progresses
                            function ( xhr ) {
                            },
                            // Function called when download errors
                            function ( xhr ) {
                                console.log('An error happened');
                                reject('xhr');
                            }
                        );
                    }));

                    //everything loaded => start creating vizualization
                    $q.all(scope.allPromises)
                        .then(function (vizData) {
                            var visualisationPromises = [];

                            var i = 0, j = 0;
                            //create portraits
                            for (i = 0; i < scope.members.length; ++i) {
                                if (scope.members[i].picture == null) {
                                    scope.members[i].picture = './photos/null'
                                }
                                modelCreator.createPortrait(scene, "./images/" +scope.members[i].picture + ".jpg", scope.members[i], visualisationPromises);
                            }
                            //create labels
                            for (i = 0; i < scope.socialInteractions.length; ++i) { //iterate over interaction types
                                for (j = 0; j < scope.socialInteractions[i].types.length; ++j) {   //iterate over element in interaction group
                                    modelCreator.createLabel(scene,
                                        "./images/labels/" + scope.socialInteractions[i].name + "/" + scope.socialInteractions[i].types[j].name + ".png",
                                        scope.socialInteractions[i].types[j], scope.socialInteractions[i].name, visualisationPromises);
                                    modelCreator.createHighlightedLabel(scene, 
                                        "./images/labels/" + scope.socialInteractions[i].name + "/highlighted." + scope.socialInteractions[i].types[j].name + ".png", 
                                        scope.socialInteractions[i].types[j], scope.socialInteractions[i].name, visualisationPromises);
                                    scope.socialInteractions[i].types[j].typeName = scope.socialInteractions[i].name;
                                    //TODO break too long labes
                                }
                            }
                            //sphereEmbedd(scene, scope.graph, scope.members, scope.socialInteractions);
                            var se = new SEAlgorithm(radA, radB, scope.graph);
                            se.computePositions(scope.members, scope.socialInteractions);

                            $q.all(visualisationPromises)
                                .then(function () {
                                    se.setPositions(scope.members, scope.socialInteractions);
                                    modelCreator.setPositions(scope.members, scope.socialInteractions);
                                    modelCreator.createEdges(scene, scope.members, scope.socialInteractions, scope.graph);
                                    modelCreator.updateFacing(camera.position, camera.up);
                                    scope.programState = programStateEnum.READY;
                                },
                                function (error) {
                                    console.error("Could not create visualization: ", error);
                                });
                        },
                        function (error) {
                            console.error("Could not load all data: ", error);
                        });
                });

                // -----------------------------------
                // Event listeners
                // -----------------------------------
                scope.onWindowResize = function () {
                    scope.resizeCanvas();
                    if (scope.navMaxHeight.height != element.parent()[0].clientHeight) {
                        scope.navMaxHeight.height = element.parent()[0].clientHeight;
                        scope.$apply();
                    }
                };

                $(window).load(function () {
                    scope.resizeCanvas();
                    scope.navMaxHeight.height = element.parent()[0].clientHeight;
                    scope.$apply();
                });

                scope.onDocumentMouseMove = function (event) {
                    if (mouseDown)
                    {
                        mauseMoved = true;
                        event.preventDefault();
                        clearInterval(rotationInterval);

                        var rect = canvas.getBoundingClientRect();
                        mouseX = event.clientX - rect.left;
                        mouseY = event.clientY - rect.top;

                        dx = (Math.PI / 180) * (0.25 * (mouseX - movingCamera.mouseX));
                        dy = (Math.PI / 180) * (0.25 * (mouseY - movingCamera.mouseY));

                        movingCamera.mouseX = mouseX;
                        movingCamera.mouseY = mouseY;

                        movingCamera.rotate(dx, dy);

                        updateCamera();
                    }
                };

                scope.onMouseClick = function (event) {
                    if (scope.programState == programStateEnum.READY) {
                        event.preventDefault();
                        mauseMoved = false;
                        mouseDown = true;

                        var rect = canvas.getBoundingClientRect();
                        movingCamera.mouseX = event.clientX - rect.left;
                        movingCamera.mouseY = event.clientY - rect.top;
                    }
                };

                scope.onMouseUnclick = function (event) {
                    if (scope.programState == programStateEnum.READY) {
                        event.preventDefault();
                        mouseDown = false;
                        if (mauseMoved == false) {
                            var mousePos = new THREE.Vector2(
                                (event.clientX - element[0].getBoundingClientRect().left - contW / 2.0 + 1) / (contW / 2.0),
                                -(event.clientY - element[0].getBoundingClientRect().top - contH / 2.0 + 1) / (contH / 2.0)
                            );
                            modelCreator.intersect(mousePos, camera, scope, syncSRV);
                        }
                    }
                };

                scope.mouseScroll = function (e) {
                    if (scope.programState == programStateEnum.READY) {
                        if (!e)
                            e = window.event;

                        var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
                        movingCamera.maginify(delta);

                        updateCamera();

                        //prevent scrolling event propagation
                        e = e || window.event;
                        if (e.preventDefault)
                            e.preventDefault();
                        e.returnValue = false;
                        //IE9 & Other Browsers
                        if (e.stopPropagation) {
                            e.stopPropagation();
                        }
                            //IE8 and Lower
                        else {
                            e.cancelBubble = true;
                        }
                        return false;
                    }
                }

                scope.onMouseDoubleClick = function (event) {
                    if (scope.programState == programStateEnum.READY) {
                        event.preventDefault();

                        var mousePos = new THREE.Vector2(
                                (event.clientX - element[0].getBoundingClientRect().left - contW / 2.0 + 1) / (contW / 2.0),
                                -(event.clientY - element[0].getBoundingClientRect().top - contH / 2.0 + 1) / (contH / 2.0)
                            );

                        var dest = modelCreator.intersect(mousePos, camera, scope, syncSRV);
                        if (dest != null) {
                            clearInterval(rotationInterval);
                            rotationDest = new THREE.Vector3().fromArray(dest.position);
                            console.log(rotationDest);
                            rotationInterval = setInterval(rotateSphereToFocus, 50);
                        }
                    }
                }

                // -----------------------------------
                // Updates
                // -----------------------------------
                scope.resizeCanvas = function () {
                    contW = element[0].clientWidth;
                    contH = element[0].clientHeight;
                    console.log("resize: " + contW + "  x  " + contH+"!");

                    windowHalfX = contW / 2;
                    windowHalfY = contH / 2;

                    canvas.width = contW;
                    canvas.height = contH;

                    camera.aspect = contW / contH;
                    camera.updateProjectionMatrix();

                    renderer.setSize(contW, contH);
                };

                scope.$watch('[__height, __width]', function () {
                    scope.resizeCanvas();
                    scope.navMaxHeight.height = element.parent()[0].clientHeight;
                }, true);

                scope.$watch('$viewContentLoaded', function () {
                    scope.resizeCanvas();
                    scope.navMaxHeight.height = element.parent()[0].clientHeight;
                }, true);

                scope.$on('updated', function () {
                    console.log('start');
                    clearInterval(rotationInterval);
                    modelCreator.newActive(syncSRV.syncData);

                    rotationDest = new THREE.Vector3(
                            syncSRV.syncData.position[0],
                            syncSRV.syncData.position[1],
                            syncSRV.syncData.position[2]
                        );
                    rotationInterval = setInterval(rotateSphereToFocus, 50);
                });

                scope.$on('startSpheresSwitch', function () {
                    clearInterval(changeInterval);
                    modelCreator.initSwitching();
                    changeInterval = setInterval(switchSpheres, 50)
                });

                scope.$on('syncGraph', function () {
                    modelCreator.updateEdges(scene, scope.members, scope.socialInteractions, scope.graph);
                });

                function updateCamera() {
                    camera.position.copy(movingCamera.position);
                    camera.up.copy(movingCamera.up);

                    modelCreator.updateFacing(camera.position, camera.up);
                    camera.lookAt(scene.position);
                }

                function rotateSphereToFocus() {
                    console.log('updating');
                    if (movingCamera.rotateTo(rotationDest, angleV)) {
                        clearInterval(rotationInterval);
                        console.log('finished');
                    } else {
                        updateCamera();
                    }
                }

                function switchSpheres() {
                    if (modelCreator.switchSpheres(radV)) {
                        clearInterval(changeInterval);
                        scope.programState = programStateEnum.READY;
                    }
                }

                // -----------------------------------
                // Draw and Animate
                // -----------------------------------
                scope.animate = function () {
                    stats.begin();
                    scope.render();
                    stats.end();
                    requestAnimationFrame(scope.animate);
                };

                scope.render = function () {
                    renderer.render(scene, camera);
                };

                // -----------------------------------
                // Begin
                // -----------------------------------
                scope.init();
                scope.animate();

            }
        };
    });

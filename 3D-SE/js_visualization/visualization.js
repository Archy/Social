﻿// -----------------------------------
// Program state
// -----------------------------------
programStateEnum = {
    READY: 0,
    LOADING: 1,
    CHANGING: 2
}

spheresStateEnum = {
    NORMAL: 0,
    SWITCHED: 1
}

// -----------------------------------
// Models handler
// -----------------------------------
function ModelsCreator(ra, rb, recursionLevel) {
    this._recursionLevel = recursionLevel;
    this._ra = ra;
    this._rb = rb;
    var t;
    var outlineWidth = 0.1;
    // http://paletton.com/#uid=7050u0ktvoiklv1pHsPwEjWGkeK

    var portraits = {};
    var portraitsBorders = {};
    var labels = {};
    var highlightedLabels = {};
    var edges = {};
    var switchedEdges = {};
    var currActive = null;

    var allObjects = [];
    var raycaster = new THREE.Raycaster();

    var spheresState = spheresStateEnum.NORMAL;

    // -----------------------------------
    // Creating portraits and labels
    // -----------------------------------

    this.createPortrait = function(scene, portraitPath, person, visualisationPromises) {
        var loader = new THREE.TextureLoader();
        //create promise
        visualisationPromises.push(new Promise(function (resolve, reject) {
            // load a resource
            loader.load(
                // resource URL
                portraitPath,
                // Function when resource is loaded
                function (texture) {
                    //create material
                    var img = new THREE.MeshBasicMaterial({ map: texture });
                    img.map.needsUpdate = true;
                    img.map.minFilter = THREE.LinearFilter; //to get rid of "The size of your texture is not powers of two" error

                    // create mesh
                    var width = 1;
                    var height = 1;
                    //keep image ascpect ratio
                    if (texture.image.width > texture.image.height) {
                        //keep height = 1, resize width
                        width = texture.image.width / texture.image.height;
                    } else {
                        //keep width = 1, resize height
                        height = texture.image.height / texture.image.width;
                    }

                    var mesh = new THREE.Mesh(new THREE.PlaneGeometry(width, height), img);
                    mesh.overdraw = true;

                    //OUTLINE
                    var outline = new THREE.Geometry();
                    var x = width / 2.0;
                    var y = height / 2.0;

                    //inner
                    outline.vertices.push(new THREE.Vector3(-x, y, 0));
                    outline.vertices.push(new THREE.Vector3(x, y, 0));
                    outline.vertices.push(new THREE.Vector3(x, -y, 0));
                    outline.vertices.push(new THREE.Vector3(-x, -y, 0));
                    x += outlineWidth;
                    y += outlineWidth;
                    //outer
                    outline.vertices.push(new THREE.Vector3(-x, y, 0));
                    outline.vertices.push(new THREE.Vector3(x, y, 0));
                    outline.vertices.push(new THREE.Vector3(x, -y, 0));
                    outline.vertices.push(new THREE.Vector3(-x, -y, 0));
                    //faces
                    //up
                    outline.faces.push(new THREE.Face3(0, 5, 4));
                    outline.faces.push(new THREE.Face3(0, 1, 5));
                    //right
                    outline.faces.push(new THREE.Face3(1, 6, 5));
                    outline.faces.push(new THREE.Face3(1, 2, 6));
                    //down
                    outline.faces.push(new THREE.Face3(7, 6, 2));
                    outline.faces.push(new THREE.Face3(7, 2, 3));
                    //left
                    outline.faces.push(new THREE.Face3(7, 3, 4));
                    outline.faces.push(new THREE.Face3(3, 0, 4));
                    //model creation
                    outline.computeFaceNormals();
                    var outlineMaterial = new THREE.MeshBasicMaterial({ color: 0x00ccff });
                    var outlineMesh = new THREE.Mesh(outline, outlineMaterial);
                    var outlineMesh = new THREE.Mesh(outline, outlineMaterial);
                    outlineMesh.visible = false;

                    //add to scene and remember
                    scene.add(mesh);
                    scene.add(outlineMesh);

                    portraits[person.name] = mesh;
                    portraitsBorders[person.name] = outlineMesh;

                    allObjects.push(mesh);
                    resolve(mesh);
                },
                // Function called when download progresses
                function (xhr) {
                },
                // Function called when download errors
                function (xhr) {
                    console.error('An error happened when loading portrait: ' + xhr);
                    reject(new Error('Could not load portrait: ' + xhr));
                }
            );
        }));
    }

    this.createLabel = function (scene, labelPath, interaction, interactionType, visualisationPromises) {
        var loader = new THREE.TextureLoader();
        //create promise
        visualisationPromises.push(new Promise(function (resolve, reject) {
            // load a resource
            loader.load(
                // resource URL
                labelPath,
                // Function when resource is loaded
                function (texture) {
                    //create material
                    var img = new THREE.MeshBasicMaterial({ map: texture, transparent: true, alphaTest: 0.5 });
                    img.map.needsUpdate = true;
                    img.map.minFilter = THREE.LinearFilter; //to get rid of "The size of your texture is not powers of two" error

                    // create mesh
                    var width = 2;
                    var height = 2;
                    //keep image ascpect ratio
                    if (texture.image.width > texture.image.height) {
                        //keep height = 1, resize width
                        width = texture.image.width / texture.image.height;
                    } else {
                        //keep width = 1, resize height
                        height = texture.image.height / texture.image.width;
                    }

                    var mesh = new THREE.Mesh(new THREE.PlaneGeometry(width, height), img);
                    mesh.overdraw = true;

                    //add to scene and remember
                    scene.add(mesh);

                    if (labels[interactionType] == null) {
                        labels[interactionType] = {};
                    }
                    labels[interactionType][interaction.name] = mesh;

                    allObjects.push(mesh);
                    resolve(mesh);
                },
                // Function called when download progresses
                function (xhr) {
                },
                // Function called when download errors
                function (xhr) {
                    console.error('An error happened when loading portrait: ' + xhr);
                    reject(new Error('Could not load portrait: ' + xhr));
                }
            );
        }));
    }

    this.createHighlightedLabel = function (scene, highlightedLabelPath, interaction, interactionType, visualisationPromises) {
        var loader = new THREE.TextureLoader();
        //create promise
        visualisationPromises.push(new Promise(function (resolve, reject) {
            // load a resource
            loader.load(
                // resource URL
                highlightedLabelPath,
                // Function when resource is loaded
                function (texture) {
                    //create material
                    var img = new THREE.MeshBasicMaterial({ map: texture, transparent: true, alphaTest: 0.5 });
                    img.map.needsUpdate = true;
                    img.map.minFilter = THREE.LinearFilter; //to get rid of "The size of your texture is not powers of two" error

                    // create mesh
                    var width = 2;
                    var height = 2;
                    //keep image ascpect ratio
                    if (texture.image.width > texture.image.height) {
                        //keep height = 1, resize width
                        width = texture.image.width / texture.image.height;
                    } else {
                        //keep width = 1, resize height
                        height = texture.image.height / texture.image.width;
                    }

                    var mesh = new THREE.Mesh(new THREE.PlaneGeometry(width, height), img);
                    mesh.overdraw = true;
                    mesh.visible = false;

                    //add to scene and remember
                    scene.add(mesh);

                    if (highlightedLabels[interactionType] == null) {
                        highlightedLabels[interactionType] = {};
                    }
                    highlightedLabels[interactionType][interaction.name] = mesh;
                    allObjects.push(mesh);
                    resolve(mesh);
                },
                // Function called when download progresses
                function (xhr) {
                },
                // Function called when download errors
                function (xhr) {
                    console.error('An error happened when loading portrait: ' + xhr);
                    reject(new Error('Could not load portrait: ' + xhr));
                }
            );
        }));
    }

    // -----------------------------------
    // Setting positions
    // -----------------------------------
    this.setPositions = function(members, socialInteractions){
        var x, y, z;
        //positions of social interaction
        for (i = 0; i < socialInteractions.length; ++i) { //iterate over interaction types
            var type = socialInteractions[i].name;
            for (j = 0; j < socialInteractions[i].types.length; ++j) {   //iterate over element in interaction group
                x = socialInteractions[i].types[j].position[0];
                y = socialInteractions[i].types[j].position[1];
                z = socialInteractions[i].types[j].position[2];

                labels[type][socialInteractions[i].types[j].name].position.set(x, y, z);
                labels[type][socialInteractions[i].types[j].name].__dirtyPosition = true;
                labels[type][socialInteractions[i].types[j].name].data = socialInteractions[i].types[j];

                highlightedLabels[type][socialInteractions[i].types[j].name].position.set(x, y, z);
                highlightedLabels[type][socialInteractions[i].types[j].name].__dirtyPosition = true;
                highlightedLabels[type][socialInteractions[i].types[j].name].data = socialInteractions[i].types[j];
            }
        }
        //positions of people
        for (i = 0; i < members.length; ++i) {
            x = members[i].position[0];
            y = members[i].position[1];
            z = members[i].position[2];
            portraits[members[i].name].position.set(x, y, z);
            portraitsBorders[members[i].name].position.set(x, y, z);
            portraits[members[i].name].__dirtyPosition = true;

            portraits[members[i].name].data = members[i];
        }
    }

    // -----------------------------------
    // Updating facing
    // -----------------------------------
    this.updateFacing = function (cameraPos, cameraUp) {
        for (var typeKey in labels) {
            for (var nameKey in labels[typeKey]) {
                labels[typeKey][nameKey].up.copy(cameraUp);
                labels[typeKey][nameKey].lookAt(cameraPos);
            }
        }

        for (var typeKey in highlightedLabels) {
            for (var nameKey in highlightedLabels[typeKey]) {
                highlightedLabels[typeKey][nameKey].up.copy(cameraUp);
                highlightedLabels[typeKey][nameKey].lookAt(cameraPos);
            }
        }

        for (var nameKey in portraits) {
            portraits[nameKey].up.copy(cameraUp);
            portraits[nameKey].lookAt(cameraPos);
        }
        for (var nameKey in portraitsBorders) {
            portraitsBorders[nameKey].up.copy(cameraUp);
            portraitsBorders[nameKey].lookAt(cameraPos);
        }
    }

    // -----------------------------------
    // Switching objects between spheres
    // -----------------------------------
    this.switchSpheres = function (radV) {
        var finished = false;

        if (spheresState == spheresStateEnum.NORMAL) {
            for (var typeKey in labels) {
                for (var nameKey in labels[typeKey]) {
                    var deltaRad = new THREE.Vector3().copy(labels[typeKey][nameKey].position).normalize().multiplyScalar(radV);
                    labels[typeKey][nameKey].position.add(deltaRad);

                    if (labels[typeKey][nameKey].position.length() >= this._rb) {
                        spheresState = spheresStateEnum.SWITCHED;
                        finished = true;
                    }
                }
            }

            for (var typeKey in highlightedLabels) {
                for (var nameKey in highlightedLabels[typeKey]) {
                    var deltaRad = new THREE.Vector3().copy(highlightedLabels[typeKey][nameKey].position).normalize().multiplyScalar(radV);
                    highlightedLabels[typeKey][nameKey].position.add(deltaRad);

                    if (highlightedLabels[typeKey][nameKey].position.length() >= this._rb) {
                        spheresState = spheresStateEnum.SWITCHED;
                        finished = true;
                    }
                }
            }

            for (var nameKey in portraits) {
                var deltaRad = new THREE.Vector3().copy(portraits[nameKey].position).normalize().multiplyScalar(-radV);
                portraits[nameKey].position.add(deltaRad);

                if (portraits[nameKey].position.length() <= this._ra) {
                    spheresState = spheresStateEnum.SWITCHED;
                    finished = true;
                }
            }

            for (var nameKey in portraitsBorders) {
                var deltaRad = new THREE.Vector3().copy(portraitsBorders[nameKey].position).normalize().multiplyScalar(-radV);
                portraitsBorders[nameKey].position.add(deltaRad);

                if (portraitsBorders[nameKey].position.length() <= this._ra) {
                    spheresState = spheresStateEnum.SWITCHED;
                    finished = true;
                }
            }

        } else {
            for (var typeKey in labels) {
                for (var nameKey in labels[typeKey]) {
                    var deltaRad = new THREE.Vector3().copy(labels[typeKey][nameKey].position).normalize().multiplyScalar(-radV);
                    labels[typeKey][nameKey].position.add(deltaRad);

                    if (labels[typeKey][nameKey].position.length() <= this._ra) {
                        spheresState = spheresStateEnum.NORMAL;
                        finished = true;
                    }
                }
            }

            for (var typeKey in highlightedLabels) {
                for (var nameKey in highlightedLabels[typeKey]) {
                    var deltaRad = new THREE.Vector3().copy(highlightedLabels[typeKey][nameKey].position).normalize().multiplyScalar(-radV);
                    highlightedLabels[typeKey][nameKey].position.add(deltaRad);

                    if (highlightedLabels[typeKey][nameKey].position.length() <= this._ra) {
                        spheresState = spheresStateEnum.NORMAL;
                        finished = true;
                    }
                }
            }

            for (var nameKey in portraits) {
                var deltaRad = new THREE.Vector3().copy(portraits[nameKey].position).normalize().multiplyScalar(radV);
                portraits[nameKey].position.add(deltaRad);

                if (portraits[nameKey].position.length() >= this._rb) {
                    spheresState = spheresStateEnum.NORMAL;
                    finished = true;
                }
            }

            for (var nameKey in portraitsBorders) {
                var deltaRad = new THREE.Vector3().copy(portraitsBorders[nameKey].position).normalize().multiplyScalar(radV);
                portraitsBorders[nameKey].position.add(deltaRad);

                if (portraitsBorders[nameKey].position.length() >= this._rb) {
                    spheresState = spheresStateEnum.NORMAL;
                    finished = true;
                }
            }
        }
        if(finished)
            this._ativateEdges()

        return finished;
    }

    this.initSwitching = function () {
        var currEdges = spheresState == spheresStateEnum.NORMAL ? edges : switchedEdges;

        if (currActive != null) {
            for (var i = 0; i < edges[currActive.name].length; ++i) {
                currEdges[currActive.name][i].visible = false;
            }
        }
    }

    this._ativateEdges = function(){
        this._activateNew();
    }

    // -----------------------------------
    // Activate object and show edges
    // -----------------------------------
    this.intersect = function (mousePos, camera, scope, syncSRV) {  //vector2D, camera
        raycaster.setFromCamera(mousePos, camera);
        var intersects = raycaster.intersectObjects(allObjects);
        if (intersects.length > 0) {
            
            this._deactivateAll();

            currActive = intersects[0].object.data;

            this._activateNew();
            syncSRV.syncScroll(currActive);
            if (currActive.typeName != null) {
                syncSRV.syncSocialInteractions(currActive.typeName);
            }
            scope.$apply();

            return currActive;
        }
        return null;
    }

    this.newActive = function (node) {

        this._deactivateAll();

        currActive = node;

        this._activateNew();
    }

    this._deactivateAll = function () {
        var currEdges = spheresState == spheresStateEnum.NORMAL ? edges : switchedEdges;

        if (currActive != null) {
            for (var i = 0; i < currEdges[currActive.name].length; ++i) {
                currEdges[currActive.name][i].visible = false;
                currEdges[currActive.name][i].member.active = false;
                currEdges[currActive.name][i].interaction.active = false;
                portraitsBorders[currEdges[currActive.name][i].member.name].visible = false;

                labels[currEdges[currActive.name][i].interaction.typeName][currEdges[currActive.name][i].interaction.name].visible = true;
                highlightedLabels[currEdges[currActive.name][i].interaction.typeName][currEdges[currActive.name][i].interaction.name].visible = false;
            }
        }
    }

    this._activateNew = function () {
        var currEdges = spheresState == spheresStateEnum.NORMAL ? edges : switchedEdges;

        for (var i = 0; i < currEdges[currActive.name].length; ++i) {
            currEdges[currActive.name][i].visible = true;
            currEdges[currActive.name][i].member.active = true;
            currEdges[currActive.name][i].interaction.active = true;
            portraitsBorders[currEdges[currActive.name][i].member.name].visible = true;

            labels[currEdges[currActive.name][i].interaction.typeName][currEdges[currActive.name][i].interaction.name].visible = false;
            highlightedLabels[currEdges[currActive.name][i].interaction.typeName][currEdges[currActive.name][i].interaction.name].visible = true;
        }
    }

    // -----------------------------------
    // Update edges
    // -----------------------------------
    this.updateEdges = function (scene, members, socialInteractions, adjacencyMatrix) {
        this._deactivateAll();

        for(edge in edges){
            scene.remove(edge);
        }
        for (edge in switchedEdges) {
            scene.remove(edge);
        }

        edges = {};
        switchedEdges = {};

        this.createEdges(scene, members, socialInteractions, adjacencyMatrix);

        this._activateNew();
    }

    // -----------------------------------
    // Creating edges
    // -----------------------------------

    this.createEdges = function (scene, members, socialInteractions, adjacencyMatrix) {
        var material = new THREE.LineBasicMaterial({
            color: 0xC1670F,
            opacity: 0.8,
            transparent: true
        });

        for (i = 0; i < members.length; ++i) {
            
            for (j = 0; j < socialInteractions.length; ++j) {
                var adjacency = adjacencyMatrix[socialInteractions[j].name][members[i].ID - 1];

                for (k = 0; k < socialInteractions[j].types.length; ++k) {
                    if (adjacency[socialInteractions[j].types[k].ID - 1]) {
                        var geometry = new THREE.Geometry();

                        var x1 = members[i].position[0];
                        var y1 = members[i].position[1];
                        var z1 = members[i].position[2];

                        var x2 = socialInteractions[j].types[k].position[0];
                        var y2 = socialInteractions[j].types[k].position[1];
                        var z2 = socialInteractions[j].types[k].position[2];

                        geometry.vertices.push(
                            new THREE.Vector3(x1, y1, z1),
                            new THREE.Vector3(x2, y2, z2) 
                        );
                        var line = new THREE.Line(geometry, material);
                        line.visible = false;
                        scene.add(line);

                        line.member = members[i];
                        line.interaction = socialInteractions[j].types[k];
                        
                        if (edges[socialInteractions[j].types[k].name] == null)
                            edges[socialInteractions[j].types[k].name] = [];
                        edges[socialInteractions[j].types[k].name].push(line);

                        if (edges[members[i].name] == null)
                            edges[members[i].name] = [];
                        edges[members[i].name].push(line);


                        //switched edges

                        geometry = new THREE.Geometry();
                        var dRadius = this._rb - this._ra;

                        var memberVector = new THREE.Vector3(x1, y1, z1);
                        var deltaRad = new THREE.Vector3().copy(memberVector).normalize().multiplyScalar(-dRadius);
                        memberVector.add(deltaRad);

                        var interactionVector = new THREE.Vector3(x2, y2, z2);
                        var deltaRad = new THREE.Vector3().copy(interactionVector).normalize().multiplyScalar(dRadius);
                        interactionVector.add(deltaRad);

                        geometry.vertices.push(
                            memberVector,
                            interactionVector
                        );
                        var line = new THREE.Line(geometry, material);
                        line.visible = false;
                        scene.add(line);

                        line.member = members[i];
                        line.interaction = socialInteractions[j].types[k];

                        if (switchedEdges[socialInteractions[j].types[k].name] == null)
                            switchedEdges[socialInteractions[j].types[k].name] = [];
                        switchedEdges[socialInteractions[j].types[k].name].push(line);

                        if (switchedEdges[members[i].name] == null)
                            switchedEdges[members[i].name] = [];
                        switchedEdges[members[i].name].push(line);
                    }
                }
            }
        }
    }
    
    // -----------------------------------
    // Creating spheres
    // -----------------------------------

    this.createSpheres = function(scene) {
        var smallerFactor = 10;
        var biggerFactor = 20;
        var material = new THREE.MeshBasicMaterial({ wireframe: true, color: 0x0B912E });
        var material2 = new THREE.MeshBasicMaterial({ wireframe: true, color: 0x0D6478 });
        var model = this._createModel();

        var smallerSphere = this._translateModel(model, this._ra);

        var biggerSphere = this._translateModel(model, this._rb);

        material.transparent = true;
        material.opacity = 0.5;
        material2.transparent = true;
        material2.opacity = 0.4;

        var smallerMesh = new THREE.Mesh(smallerSphere, material);
        var biggerMesh = new THREE.Mesh(biggerSphere, material2);

        scene.add(smallerMesh);
        scene.add(biggerMesh);
    }

    this._createModel = function() {
        //model creation
        var geometry = new THREE.Geometry();
        var i, j, face;
        t = (1.0 + Math.sqrt(5.0)) / 2.0;

        var isohedronVertices = [
            -1, t, 0, 1, t, 0,
            -1, -t, 0, 1, -t, 0,
            0, -1, t, 0, 1, t,
            0, -1, -t, 0, 1, -t,
            t, 0, -1, t, 0, 1,
            -t, 0, -1, -t, 0, 1
        ];

        for (i = 0; i < isohedronVertices.length; i += 3) {
            this._addVertex(
                isohedronVertices[i],
                isohedronVertices[i + 1],
                isohedronVertices[i + 2],
                geometry);
        }

        geometry.faces.push(new THREE.Face3(0, 11, 5));
        geometry.faces.push(new THREE.Face3(0, 5, 1));
        geometry.faces.push(new THREE.Face3(0, 1, 7));
        geometry.faces.push(new THREE.Face3(0, 7, 10));
        geometry.faces.push(new THREE.Face3(0, 10, 11));

        geometry.faces.push(new THREE.Face3(1, 5, 9));
        geometry.faces.push(new THREE.Face3(5, 11, 4));
        geometry.faces.push(new THREE.Face3(11, 10, 2));
        geometry.faces.push(new THREE.Face3(10, 7, 6));
        geometry.faces.push(new THREE.Face3(7, 1, 8));

        geometry.faces.push(new THREE.Face3(3, 9, 4));
        geometry.faces.push(new THREE.Face3(3, 4, 2));
        geometry.faces.push(new THREE.Face3(3, 2, 6));
        geometry.faces.push(new THREE.Face3(3, 6, 8));
        geometry.faces.push(new THREE.Face3(3, 8, 9));

        geometry.faces.push(new THREE.Face3(4, 9, 5));
        geometry.faces.push(new THREE.Face3(2, 4, 11));
        geometry.faces.push(new THREE.Face3(6, 2, 10));
        geometry.faces.push(new THREE.Face3(8, 6, 7));
        geometry.faces.push(new THREE.Face3(9, 8, 1));

        for (var i = 0; i < recursionLevel; i++) {
            var length = geometry.faces.length;
            var faces2 = [];
            for (var j = 0; j < length; j++) {

                var face = geometry.faces[j];

                var v1 = geometry.vertices[face.a];
                var v2 = geometry.vertices[face.b];
                var v3 = geometry.vertices[face.c];

                var midPoint1 = this._createGetMiddlePointIndex(v1, v2, geometry);
                var midPoint2 = this._createGetMiddlePointIndex(v2, v3, geometry);
                var midPoint3 = this._createGetMiddlePointIndex(v3, v1, geometry);

                faces2.push(new THREE.Face3(face.a, midPoint1, midPoint3));
                faces2.push(new THREE.Face3(face.b, midPoint2, midPoint1));
                faces2.push(new THREE.Face3(face.c, midPoint3, midPoint2));
                faces2.push(new THREE.Face3(midPoint1, midPoint2, midPoint3));
            }
            geometry.faces = faces2;
        }

        geometry.computeFaceNormals();

        return geometry;
    }

    this._createGetMiddlePointIndex = function(p1, p2, geometry) {
        var x = (p1.x + p2.x) / 2;
        var y = (p1.y + p2.y) / 2;
        var z = (p1.z + p2.z) / 2;

        var point = this._addVertex(x, y, z, geometry);

        return geometry.vertices.indexOf(point);
    }

    this._addVertex = function(x, y, z, geometry) {

        var placeOnSphere = Math.sqrt(x * x + y * y + z * z);
        x = x * t / placeOnSphere;
        y = y * t / placeOnSphere;
        z = z * t / placeOnSphere;

        var point = new THREE.Vector3(x, y, z);

        if (geometry.vertices.indexOf(point) == -1)
            geometry.vertices.push(point);

        return point;
    }

    this._translateModel = function(geometry, factor) {
        for (i = 0; i < geometry.vertices.length; i++) {
            var x = geometry.vertices[i].x;
            var y = geometry.vertices[i].y;
            var z = geometry.vertices[i].z;
            var length = math.sqrt(x * x + y * y + z * z);
            var scale = factor / length;

            geometry.vertices[i].x *= scale;
            geometry.vertices[i].y *= scale;
            geometry.vertices[i].z *= scale;
        }

        return new THREE.BufferGeometry().fromGeometry(geometry);
    }

}

// -----------------------------------
// Camera
// -----------------------------------
function movingCameraClass() {
    this.x = 0.0;
    this.y = 0.0;
    this.z = 10.0;
    this.mouseX = 0;
    this.mouseY = 0;

    this.up = new THREE.Vector3(0, 1, 0);
    this.position = new THREE.Vector3(0, 1, -70);

    this.MAX_RADIUS = 500.0;
    this.MIN_RADIUS = 3.0;

    this.maginify = function (dRadius) {
        var deltaRad = new THREE.Vector3().copy(this.position).normalize().multiplyScalar(-dRadius);
        this.position.add(deltaRad);
    };

    this.rotate = function (dTheta, dPhi) {
        //rotate vertiacally
        var rotationAxis = new THREE.Vector3().crossVectors(this.position, this.up).normalize();
        var rotationMatrix = new THREE.Matrix4().makeRotationAxis(rotationAxis, dPhi);
        this.position.applyMatrix4(rotationMatrix);
        this.up.crossVectors(rotationAxis, this.position).normalize();

        //rotate horizontally
        rotationMatrix = new THREE.Matrix4().makeRotationAxis(this.up, -dTheta);
        this.position.applyMatrix4(rotationMatrix);
    };

    this.rotateTo = function (dest, angleV) {
        var angle = this.position.angleTo(dest);
        if (angle < angleV)
            return true;
        
        var rotationAxis = new THREE.Vector3().crossVectors(this.position, dest).normalize();
        var rotationMatrix = new THREE.Matrix4().makeRotationAxis(rotationAxis, angleV);
        this.position.applyMatrix4(rotationMatrix);
        this.up.projectOnPlane(this.position).normalize();
        return false;
    }
}

﻿// M - number of social interactions
// N - number of people

// Inner: Va = {1 ... M}; X = M x 3;    -- socialInteractions
// Outer: Vb = {1 ... N}; Y = N x 3;    -- members

// Adjacency: (M+N) x (M+N) (interactions+members) {-1, 1}

function SEAlgorithm(ra, rb, adjacency){
    this._ra = ra;
    this._rb = rb;
    this._adjacency = adjacency;
    this._M = 0;
    this._N = 0;
    this._positions = [];

    this._degree = Math.PI * 1.0/16.0;

    this.computePositions = function(members, socialInteractions){
        this._N = this._countPeople(members);
        this._M = this._countSocialInteractions(socialInteractions);
        // 1. create initial positions vector
        this._positions = this._createPositionsVector();
        // 2. iterate algorith:
        for(var iteration=0; iteration<30; ++iteration){
            // a) create vector for remembering gradient
            grad = this._createGradientArray();
            // b) iterate over ALL pairs of nodes and count gradient
            for(var i=0; i<= (this._M+this._N-2); ++i){
                for(var j=i+1; j <= (this._M+this._N-1); ++j){
                    var Xi =  this._positions[i];
                    var Xj =  this._positions[j];
                    var sum = this._getArr(i, j, members, socialInteractions) -  Xi[0] * Xj[0] - Xi[1] * Xj[1] - Xi[2] * Xj[2];
                    //compute and remember partial gradients
                    //grad Xi
                    grad[i][0] -= sum * Xj[0];
                    grad[i][1] -= sum * Xj[1];
                    grad[i][2] -= sum * Xj[2];
                    //grad Xj
                    grad[j][0] -= sum * Xi[0];
                    grad[j][1] -= sum * Xi[1];
                    grad[j][2] -= sum * Xi[2];
                }
            }
            // c) update positions
            for (var i = 0; i < this._N + this._M; ++i) {
                var g = grad[i];
                var pos = this._positions[i];

                //var coeff = 0.0000000001;
                //var x = pos[0] - coeff * g[0];
                //var y = pos[1] - coeff * g[1];
                //var z = pos[2] - coeff * g[2];

                //var radius = (i < this._M) ? this._ra : this._rb;
                //var newPos = [
                //    radius / (Math.sqrt(1 + (y / x) * (y / x) + (z / x) * (z / x))),
                //    radius / (Math.sqrt(1 + (x / y) * (x / y) + (z / y) * (z / y))),
                //    radius / (Math.sqrt(1 + (x / z) * (x / z) + (y / z) * (y / z)))
                //];
                //this._positions[i] = newPos;

                //project gradient onto the tangent plane
                var dotProduct = math.dot(g, pos);
                var temp = [dotProduct*pos[0], dotProduct*pos[1], dotProduct*pos[2]];
                var h = [
                    g[0] - temp[0],
                    g[1] - temp[1],
                    g[2] - temp[2]
                ];
                // normalize it
                var radius = (i < this._M) ? this._ra : this._rb;
                var n = [
                    radius / Math.sqrt(1 + (h[1] / h[0]) * (h[1] / h[0]) + (h[2] / h[0]) * (h[2] / h[0])),
                    radius / Math.sqrt(1 + (h[0] / h[1]) * (h[0] / h[1]) + (h[2] / h[1]) * (h[2] / h[1])),
                    radius / Math.sqrt(1 + (h[1] / h[2]) * (h[1] / h[2]) + (h[0] / h[2]) * (h[0] / h[2]))
                ];
                //count new positions
                this._degree = 0.0001;
                console.log(Math.cos(this._degree) + "  " + Math.sin(this._degree));
                var newPos = [
                    this._positions[i][0] * Math.cos(this._degree) + n[0] * Math.sin(this._degree),
                    this._positions[i][1] * Math.cos(this._degree) + n[1] * Math.sin(this._degree),
                    this._positions[i][2] * Math.cos(this._degree) + n[2] * Math.sin(this._degree)
                ];
                this._positions[i] = newPos;
            }

        }

        console.info('algoritm finished');
    }

    // -----------------------------------
    // constraint gradient descent functions
    // ----------------------------------
    this._getArr = function(i, j, members, socialInteractions){
        var ri, rj = 0;
        var a = 0;
        //radii
        if(i < this._M){
            ri = this._ra;
        } else {
            ri = this._rb;
        }
        if(j < this._M){
            rj = this._ra;
        } else {
            rj = this._rb;
        }
        //adjacency
        if ((i<this._M && j<this._M) || (this._M<=i && this._M<=j)){
            //the same sphere
            a = -1.0;
        } else {
            var typeIndex= Math.min(i, j);
            var membersIndex = Math.max(i, j) - this._M;
            var interaction = {}; 

            for(var i=0; i < socialInteractions.length; ++i){

                if(typeIndex < socialInteractions[i].types.length){
                    interaction = socialInteractions[i];
                    break;
                }
                else
                    typeIndex -= socialInteractions[i].types.length;
            }

            if((this._adjacency[interaction.name][members[membersIndex].ID -1])[interaction.types[typeIndex].ID - 1] == true){
                a = 1.0;
            } else {
                a = -1.0;
            }
        }
        if (a * ri * rj == 0)
            console.error("Something went terribly wron");
        return a*ri*rj;
    }

    // -----------------------------------
    // Positions vector creation
    // ----------------------------------
    this._createPositionsVector = function(){
        var size = this._N + this._M;
        var X = new Array(size);
        var theta, phi, x, y, z;

        //inital positions of social interaction
        for (var i = 0; i < this._M; ++i) {
            theta = math.random() * 2.0 * Math.PI;
            phi = math.random() * (Math.PI * 0.75) + (Math.PI * 1.0/8.0);
            //count 3d coordinates
            x = this._ra * Math.sin(phi) * Math.cos(theta);
            y = this._ra * Math.cos(phi);
            z = this._ra * Math.sin(phi) * Math.sin(theta);
            X[i] = [x, y, z];
        }

        //inital positions of people
        for (var i = 0; i < this._N; ++i) {
            theta = math.random() * 2.0 * Math.PI;
            phi = math.random() * (Math.PI * 0.75) + (Math.PI * 1.0 / 8.0);
            //count 3d coordinates
            x = this._rb * Math.sin(phi) * Math.cos(theta);
            y = this._rb * Math.cos(phi);
            z = this._rb * Math.sin(phi) * Math.sin(theta);
            X[i+this._M] = [x, y, z];
        }

        return X;
    }

    this._createGradientArray = function(){
        var X = new Array(this._N + this._M);
        for (var i = 0; i < this._N+this._M; ++i) {
            X[i] = [0.0, 0.0, 0.0];
        }
        return X;
    }

    // -----------------------------------
    // Setting webgl object positions
    // -----------------------------------
    this.setPositions = function (members, socialInteractions) {
        var x, y, z;
        //positions of social interaction
        var index = 0;
        for (i = 0; i < socialInteractions.length; ++i) { //iterate over interaction types
            for (j = 0; j < socialInteractions[i].types.length; ++j) {   //iterate over element in interaction group
                x = this._positions[index][0];
                y = this._positions[index][1];
                z = this._positions[index][2];
                socialInteractions[i].types[j].position = [x, y, z];
                ++index;
            }
        }

        //positions of people
        for (i = 0; i < members.length; ++i) {
            x = this._positions[index][0];
            y = this._positions[index][1];
            z = this._positions[index][2];
            members[i].position = [x, y, z];
            ++index;
        }
    }

    // -----------------------------------
    // Elements counting
    // -----------------------------------
    this._countPeople = function (members) {
        return members.length;
    }

    this._countSocialInteractions = function(socialInteractions) {
        var counter = 0;
        for (i = 0; i < socialInteractions.length; ++i) { //iterate over interaction types
            counter += socialInteractions[i].types.length;
        }
        return counter;
    }
}
﻿angular.module('socialViz')
    .directive('ngAdjacency', function ($q, syncSRV, $compile) {
        return {
            restrict: 'EA',
            link: function postLink(scope, element, attrs) {
                // -----------------------------------
                // Promises 
                // -----------------------------------
                scope.$watch('$viewContentLoaded', function () {    //prevents $q.all being fired on empty promises array
                    $q.all(scope.allPromises)
                        .then(function (vizData) {
                            var newDirective = angular.element(
                                '<table class="table-bordered" id="adjacencyTable">' +
                                    '<thead>' + 
                                        '<tr>' +
                                            '<th></th>' +
                                            '<th  ng-repeat="member in members" >{{member.name}}</th>' +
                                          '</tr>' +
                                    '</thead>' +
                                    '<tbody ng-repeat="interactionGroup in socialInteractions">' +
                                        '<tr ng-repeat="interaction in interactionGroup.types">' +
                                            '<th style="left: 0px; position: relative;" >{{interaction.name}}</th>' +
                                            '<td  ng-repeat="member in members">' +
                                                '<input type="checkbox"' +
                                                    'ng-model="graph[interactionGroup.name][member.ID -1][interaction.ID - 1]"' + 
                                                    'ng-click="updateGraph()">' +
                                            '</td>' +
                                        '</tr>' +
                                    '</tbody>' +
                                '</table>'
                            );

                            element.append(newDirective);
                            $compile(newDirective)(scope);

                            setTimeout(fixHeaderAndColumn, 500);
                        },
                        function (error) {
                            console.error("Could not load all data: ", error);
                        });
                }, true);

                var fixHeaderAndColumn = function () {
                    $("#adjacencyTable").tableHeadFixer({ "left": 1 });
                }

                scope.updateGraph = function () {
                    syncSRV.syncGraph();
                }
            }
        };
    });

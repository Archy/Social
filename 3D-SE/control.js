﻿var vizModule = angular.module('socialViz', []);

vizModule.service('syncSRV', function ($rootScope) {
    "use strict";
    this.sync = function (data) {
        this.syncData = data;
        $rootScope.$broadcast('updated');
    };

    this.syncScroll = function (data) {
        this.scrollTo = data;
        $rootScope.$broadcast('updatedScroll');
    }

    this.syncSocialInteractions = function (type) {
        this.interactionType = type;
        $rootScope.$broadcast('updatedSocialInteraction');
    }

    this.beginSpheresSwitch = function () {
        $rootScope.$broadcast('startSpheresSwitch');
    }

    this.syncGraph = function () {
        $rootScope.$broadcast('syncGraph');
    }
});

vizModule.controller('mainController', function ($scope, $http, syncSRV) {
    $scope.members = [];
    $scope.socialInteractions = [];
    $scope.graph = {};
    $scope.navMaxHeight = { height: 0 };

    $scope.allPromises = null;
    $scope.nextActive = null;

    $scope.nextWindow = "adjacency";

    $scope.programState = programStateEnum.LOADING;

    $scope.$watch('$viewContentLoaded', function () {
        $scope.allPromises = [];
        $scope.allPromises.push($http.get('data/people.json')
            .success(function (data, status, headers, config) {
                console.log("loaded people data");
                $scope.members = data;
                //create and set active to false
                for (i = 0; i < $scope.members.length; ++i) {
                    $scope.members[i].active = false;
                }
            }).error(function (data, status, headers, config) {
                console.error("FAILED TO load people data");
            })
        );
        
        $scope.allPromises.push($http.get('data/interactions.json')
            .success(function (data, status, headers, config) {
                console.log("loaded interactions json");
                $scope.socialInteractions = data;
                //create and set active to false
                for (i = 0; i < $scope.socialInteractions.length; ++i) {
                    $scope.socialInteractions[i].active = false;
                    $scope.socialInteractions[i].filter = '';

                    //create and set active to false to all members of category
                    for (j = 0; j < $scope.socialInteractions[i].types.length; ++j) {
                        $scope.socialInteractions[i].types[j].active = false;
                    }
                }
                //set active type of social interactions
                s = $scope.socialInteractions[0];

                s.active = !s.active;
                $scope.currActive = s;

                $scope.socialInteraction = s.types;
                $scope.interactionsFilter = s.filter;
            }).error(function (data, status, headers, config) {
                console.error("FAILED TO load social interactions data");
            })
        );

        $scope.allPromises.push($http.get('data/graph.json')
            .success(function (data, status, headers, config) {
                console.log("loaded graphs json");
                $scope.graph = data;
            }).error(function (data, status, headers, config) {
                console.error("FAILED TO load social interactions data");
            })
        );
    });

    $scope.swapSpheres = function () {
        if ($scope.programState == programStateEnum.READY) {
            $scope.programState = programStateEnum.CHANGING;
            syncSRV.beginSpheresSwitch();
        }
    }

    $scope.swapVizAdj = function () {
        if ($scope.programState == programStateEnum.READY) {
            if ($scope.nextWindow == "adjacency") {
                $scope.nextWindow = "visualization";
                $("#webglContainer").css("display", "none");
                $("#adjacencyContainer").css("display", "block");
            } else {
                $scope.nextWindow = "adjacency";
                $("#webglContainer").css("display", "block");
                $("#adjacencyContainer").css("display", "none");
            }
        }
    }
})

vizModule.directive("ngLeftNav", function (syncSRV) {
    return {
        restrict: "EA",
        controller: ["$scope", function ($scope) {
            $scope.dataTableHeightLeft = '500px';
        }],
        link: function postLink(scope, element, attrs) {
            scope.$watch('navMaxHeight', function (newValue, oldValue) {
                var totalHeight = 0;
                for (var i = 0; i < 3; i++) {
                    var currDiv = angular.element(element)[0].children[i];
                    totalHeight += $(currDiv).outerHeight(true);
                }

                var dataHeight = scope.navMaxHeight.height - totalHeight;
                var dataTable = element[0].getElementsByClassName('dataTable');
                scope.dataTableHeightLeft = dataHeight + 'px';
            }, true);

            scope.setActiveLeft = function (s) {
                if (scope.programState == programStateEnum.READY) {
                    syncSRV.sync(s);
                }
            };

            scope.$on('updatedScroll', function () {
                var dataList = (element[0].querySelector('.dataTable'));
                var liList = dataList.querySelector('ul');
                var liObjects = dataList.getElementsByTagName("li");
                for (i = 0; i < liObjects.length; i++) {
                    var li = liObjects[i];
                    if (li.innerText == syncSRV.scrollTo.name.toUpperCase()) {
                        var liListOffset = $(liList).offset();
                        var offset = $(li).offset();
                        var height = $(li).outerHeight(true);
                        $(dataList).scrollTop(offset.top - liListOffset.top - height);
                        break;
                    }

                }
            });
        }
    }
});

vizModule.directive("ngRightNav", function (syncSRV) {
    return {
        restrict: "EA",
        controller: ["$scope", function ($scope) {
            $scope.socialInteraction = [];
            $scope.currActive = {};
            $scope.dataTableHeightRight = '500px';

            $scope.click = function (s) {
                if ($scope.programState == programStateEnum.READY) {
                    if ($scope.currActive !== null) {
                        $scope.currActive.filter = $scope.interactionsFilter;
                    }

                    s.active = !s.active;
                    $scope.currActive = s;

                    $scope.socialInteraction = s.types;
                    $scope.interactionsFilter = s.filter;
                }
            };
        }],
        link: function postLink(scope, element, attrs) {
            scope.$watch('[navMaxHeight, currActive]', function (newValue, oldValue) {
                var totalHeight = 0;
                for (var i = 0; i < 3; i++) {
                    var currDiv = angular.element(element)[0].children[i];
                    totalHeight += $(currDiv).outerHeight(true);
                }

                var dataHeight = scope.navMaxHeight.height - totalHeight;
                var dataTable = element[0].getElementsByClassName('dataTable');
                scope.dataTableHeightRight = dataHeight + 'px';
            }, true);

            scope.setActiveRight = function (s) {
                if (scope.programState == programStateEnum.READY) {
                    syncSRV.sync(s);
     
                }
            };

            scope.$on('updatedScroll', function () {
                var dataList = (element[0].querySelector('.dataTable'));
                var liList = dataList.querySelector('ul');
                var liObjects = dataList.getElementsByTagName("li");
                for (i = 0; i < liObjects.length; i++) {
                    var li = liObjects[i];
                    if (li.innerText == syncSRV.scrollTo.name.toUpperCase()) {
                        dataList.scrollTop = $(li).offset().top - $(li).outerHeight(true);
                        break;
                    }

                }
            });

            scope.$on("updatedSocialInteraction", function () {
                s = null;
                for (i = 0; i < scope.socialInteractions.length; ++i) {
                    if (scope.socialInteractions[i].name == syncSRV.interactionType) {
                        s = scope.socialInteractions[i];
                        break;
                    }
                }
                console.log(syncSRV.interactionType);


                if (scope.currActive !== null) {
                    scope.currActive.filter = scope.interactionsFilter;
                }

                s.active = !s.active;
                scope.currActive = s;

                scope.socialInteraction = s.types;
                scope.interactionsFilter = s.filter;

                scope.$apply();
            });
        }
    }
});
